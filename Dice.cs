
namespace ConsoleApplication
{
    public class Dice
    {
        public static int Roll(int quantity, int type)
        {
            int i;
            int valueRoll = 0;
            for (i = 1; i <= quantity; i++)
            {
                valueRoll += Builder.getRandom.Next(1, type);
            }
            return valueRoll;
        }
        public static int SystemRoll()
        {
            return Builder.getRandom.Next(1, 20);
        }
        public static bool Haste()
        {
            int random = Builder.getRandom.Next(1,20);
            if (random > 10) return true; else return false;
        }
    }
}