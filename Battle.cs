using System;

namespace ConsoleApplication
{
    public class Battle
    {
        public static void StartBattle()
        {
            string monsterString = Monsters.SummonMonster();
            string[] monsterArray = monsterString.Split(',');
            string monsterName = monsterArray[0];
            int monsterHP = Convert.ToInt32(monsterArray[1]);
            string monsterAttack = monsterArray[2];
            string[] dmgRoll = monsterArray[3].Split('d');
            int diceCount = Convert.ToInt32(dmgRoll[0]);
            int diceSize = Convert.ToInt32(dmgRoll[1]);
            Console.WriteLine("{0} has entered the battle zone. {1} HP.", monsterName, monsterHP);
            if (Dice.Haste() == true) Console.WriteLine("{0} quick attacked!",monsterName);
            int hasteDMG = Dice.Roll(1,12);
            Hero.hitPoints -= hasteDMG;
            Console.WriteLine("{0} attacked with {1}!",monsterName,monsterAttack);
            Console.WriteLine("You have received {0} damage, HP: {1}",hasteDMG,Hero.hitPoints);
            BattleSequence(monsterString);
        }
        public static void BattleSequence(string monster)
        {
            string[] monsterArray = monster.Split(',');
            string monsterName = monsterArray[0];
            int monsterHP = Convert.ToInt32(monsterArray[1]);
            string monsterAttack = monsterArray[2];
            string[] dmgRoll = monsterArray[3].Split('d');
            int diceCount = Convert.ToInt32(dmgRoll[0]);
            int diceSize = Convert.ToInt32(dmgRoll[1]);
            string[] attack = Hero.Attack.Split(',');
            string[] heavyAttack = Hero.HeavyAttack.Split(',');
            string[] defend = Hero.Defend.Split(',');
            string[] heal = Hero.Heal.Split(',');
            Console.WriteLine("It is now your turn.");
            Console.WriteLine(attack[0]+" | "+heavyAttack[0]+" | "+defend[0]+" | "+heal[0]);
            Console.WriteLine("Please type an ability, or type info for help");
            string option = Console.ReadLine();
            if (option == attack[0])
            {
                string[] rollString = attack[1].Split('d');
                int[] Roll = new int[2];
                for (int i = 0; i < 2; i++)
                {
                    Roll[i] = Convert.ToInt32(rollString[i]);
                }
                int heroDMG = Dice.Roll(Roll[0],Roll[1]);
                Console.WriteLine("{0} attacks with {1} for {2} damage to {3}.",Hero.charName,attack[0],heroDMG,monsterName);
            }
            else if (option == heavyAttack[0])
            {

            }
            else if (option == defend[0])
            {

            }
            else if (option == heal[0])
            {

            }
            else if (option == "info") 
            {

            }
            else
            {
                Console.WriteLine("Invalid Input. Please try again.");
                BattleSequence(monster);
            }
        }
    }
}