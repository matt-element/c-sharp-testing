using System;
using System.Collections.Generic;

namespace ConsoleApplication
{
    public class Builder
    {
        public static Random getRandom = new Random();
        public static int maxLength = Hero.charLevel * 5;
        public static int worldLength = getRandom.Next(5, maxLength);
        public static string Adjective()
        {
            string[] adjective = { "gleaming", "dark", "filthy", "desolate", "bright", "deserted", "beautiful", "bustling", "crowded", "ruined", "crumbling", "modern" };
            int length = adjective.Length;
            length -= 1;
            int index = getRandom.Next(0, length);
            return adjective[index];
        }
        public static string WorldType()
        {
            string[] levelType = { "city", "forest", "town", "grassland", "castle", "mountain", "desert", "valley", "village", "jungle" };
            int length = levelType.Length;
            length -= 1;
            int index = getRandom.Next(0, length);
            return levelType[index];
        }
        public static string Weather()
        {
            string[] weather = { "clear", "foggy", "rainy", "cloudy", "snowy", "arid", "wet", "cloudy" };
            int length = weather.Length;
            length -= 1;
            int index = getRandom.Next(0, length);
            return weather[index];
        }
        public static bool Sense()
        {
            int playerRoll = Dice.SystemRoll();
            int systemRoll = Dice.SystemRoll();
            int modifier = Hero.Int;
            playerRoll += modifier;
            if (playerRoll > systemRoll) {
                 return true; 
            }
            else
            {
                return false;
            }
        }
        public static string SenseList()
        {
            List<string> senses = new List<string>();
            senses.Add("there is a trail of blood on the path leading forward.");
            senses.Add("a group of bandits up ahead.");
            senses.Add("someone is watching you.");
            senses.Add("an evil presence is near.");
            return senses[getRandom.Next(0,senses.Count-1)];
        }
        public static bool Ambush()
        {
            int modifier = Hero.Dex;
            int systemRoll = Dice.SystemRoll();
            int playerRoll = Dice.SystemRoll() + modifier;
            if (playerRoll > systemRoll)
            {
                return false;
            }
            else
            {
                return true;
            } 
        }
    }
    public class World
    {
        public static void MakeWorld(string weather, string adjective, string levelType)
        {
            Console.WriteLine("You have entered a "+adjective+" "+levelType+", it is currently "+weather+" outsite.");
            if (Builder.Sense() == true) { Console.WriteLine("You sense " + Builder.SenseList()); }
            else { Console.WriteLine("You cannot sense anything."); Actions(levelType); }
            if (Builder.Ambush() == true) { Console.WriteLine("You have been Ambushed!!"); Battle.StartBattle(); }
            else { Actions(levelType); }
        }
        public static void Actions(string levelType)
        {
            List<string> actions = new List<string>();
            actions.Add("1. Continue forward");
            actions.Add("2. Search for beasts");
            actions.Add("3. Forage to restore HP.");
            actions.Add("4. Look for shops");
            actions.Add("5. Find an Inn to Rest");
            actions.Add("6. Display My Stats.");
            actions.Add("7. Save Game");
            actions.Add("8. Quit Game.");

            Console.WriteLine("Please choose an action:");
            foreach (string action in actions)
            {
                Console.WriteLine(action);
            }
            int option = Convert.ToInt32(Console.ReadLine());
            switch(option)
            {
                case 1:
                Forward(levelType);
                break;
                case 2:
                SearchBeasts();
                break;
                case 3:
                break;
                case 4:
                break;
                case 5:
                break;
                case 6:
                break;
                case 7:
                break;
                case 8:
                break;
            }
        }
        public static void Forward(string levelType)
        {
            Console.WriteLine("You continue through the "+levelType+".");
            Builder.Ambush();

        }
        public static void SearchBeasts()
        {
            Console.WriteLine("You start searching for any beasts in the local area.");
        }
    }
}