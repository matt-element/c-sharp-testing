using System.Collections.Generic;

namespace ConsoleApplication
{
    public class Monsters
    {
        public static string SummonMonster()
        {
            List<string> monsters = new List<string>();
            monsters.Add("Giant Rat,40,Scratch,1d6");
            monsters.Add("Scorpion,30,Sting,1d12");
            monsters.Add("Raider,25,Slash,1d12");
            monsters.Add("Basilisk,70,Tail Swipe,2d6");
            monsters.Add("Yao Guai,100,Slash,1d6");
            monsters.Add("Centaur,25,Stomp,1d6");
            monsters.Add("Couatl,50,Swoop,2d6");
            monsters.Add("Bone Devil,40,Slash,1d6");
            monsters.Add("Sneaky Rogue,50,Backstab,1d12");
            monsters.Add("Possessed Soldier,60,Slash,1d12");
            monsters.Add("Imp,35,Fireball,2d6");
            monsters.Add("Reaver,50,Flame Balde,3d12");
            monsters.Add("Bog Monster,75,Boil,3d6");
            monsters.Add("Deathclaw,400,Swipe,2d12");
            monsters.Add("Rivenwing,120,Wing Tornado,1d12");
            monsters.Add("Thresher Maw,1000,Bury,3d12");
            int index = Builder.getRandom.Next(1, monsters.Count-1);
            return monsters[index];
        }
    }
}