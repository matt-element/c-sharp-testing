using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static Builder Builder = new Builder();
        public static string weather = Builder.Weather();
        public static string adjective = Builder.Adjective();
        public static string levelType = Builder.WorldType();
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter Character Name:");
            Hero.SetName(Console.ReadLine());
            Hero.SetRank("Recruit");
            Console.WriteLine("Welcome to the Warden's Militia, " + Hero.charName + "!");
            Console.WriteLine("You have been assigned the rank of " + Hero.Rank);
            Hero.SetStats();
            bool isValid = true;
            do
            {
                string agree = Console.ReadLine();
                isValid = true;
                switch (agree)
                {
                    case "Y":
                        Hero.charLevel = 1;
                        isValid = true;
                        break;
                    case "N":
                        Hero.SetStats();
                        isValid = false;
                        break;
                    default:
                        Console.WriteLine("Invalid Character. Please try again.");
                        isValid = false;
                        break;
                }

            } while (!isValid);
            Hero.SetClass();
            Console.WriteLine("Let the adventure begin!");
            World.MakeWorld(weather, adjective, levelType);
        }
    }
}
