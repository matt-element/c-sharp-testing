using System;

namespace ConsoleApplication
{
    public class Hero
    {
        public static string charName;
        public static string Rank;
        public static string charClass;
        public static int Str = 0;
        public static int Con = 0;
        public static int Dex = 0;
        public static int Int = 0;
        public static int Wis = 0;
        public static int Cha = 0;
        public static int charLevel = 0;
        public static int points = 12;
        public static int hitPoints = 100;
        public static int magicPoints = 20;

        public static string Attack;
        public static string HeavyAttack;
        public static string Defend;
        public static string Heal;
        public static void SetName(string newName)
        {
            charName = newName;
        }
        public static void SetRank(string newRank)
        {
            Rank = newRank;
        }
        public static void SetStats()
        {
            Hero.Str = 0;
            Hero.Con = 0;
            Hero.Dex = 0;
            Hero.Int = 0;
            Hero.Wis = 0;
            Hero.Cha = 0;
            Hero.points = 12;
            Console.WriteLine("Please set your stat values, you have " + Hero.points + " points available.");
            while (Hero.points <= 12)
            {
                Console.WriteLine("Please Choose a Stength (STR) Value between 0 and " + Hero.points +". Strength measures your character’s muscle and physical power.");
                int str1 = Convert.ToInt32(Console.ReadLine());
                Hero.points -= str1;
                Hero.Str = str1;
                if (Hero.points == 0) { break; }
                Console.WriteLine("Please Choose a Constitution (CON) Value between 0 and " + Hero.points+ ". Constitution represents your character’s health and stamina.");
                int con1 = Convert.ToInt32(Console.ReadLine());
                Hero.points -= con1;
                Hero.Con = con1;
                if (Hero.points == 0) { break; }
                Console.WriteLine("Please Choose a Dexterity (DEX) Value between 0 and " + Hero.points + ". Dexterity measures hand-eye coordination, agility, reflexes, and balance.");
                int dex1 = Convert.ToInt32(Console.ReadLine());
                Hero.points -= dex1;
                Hero.Dex = dex1;
                if (Hero.points == 0) { break; }
                Console.WriteLine("Please Choose a Intelligence (INT) Value between 0 and " + Hero.points + ". Intelligence determines how well your character learns and reasons.");
                int int1 = Convert.ToInt32(Console.ReadLine());
                Hero.points -= int1;
                Hero.Int = int1;
                if (Hero.points == 0) { break; }
                Console.WriteLine("Please Choose a Wisdom (WIS) Value between 0 and " + Hero.points + ". Wisdom describes a character’s willpower, common sense, perception, and intuition.");
                int wis1 = Convert.ToInt32(Console.ReadLine());
                Hero.points -= wis1;
                Hero.Wis = wis1;
                if (Hero.points == 0) { break; }
                Console.WriteLine("Please Choose a Charisma (CHA) Value between 1 and " + Hero.points + ". Charisma measures a character’s force of personality, persuasiveness, personal magnetism, ability to lead, and physical attractiveness.");
                int cha1 = Convert.ToInt32(Console.ReadLine());
                Hero.points -= cha1;
                Hero.Cha = cha1;
                if (Hero.points == 0) { break; }
            }
            Console.WriteLine("Your stat values are:");
            Console.WriteLine("STR:" + Hero.Str + " CON:" + Hero.Con + " DEX:" + Hero.Dex + " INT:" + Hero.Int + " WIS:" + Hero.Wis + " CHA:" + Hero.Cha);
            Console.WriteLine("Do you want to keep these values? Y/N");
        }
        public static void SetClass()
        {
            Console.WriteLine("Choose your Character's Class:");
            Console.WriteLine("Warrior, Paladin, Mage, Warlock");
            string input = Console.ReadLine();
            switch(input)
            {
                case "Warrior":
                Hero.charClass = "Warrior";
                Hero.Str += 1;
                Hero.Attack = "Slash,3d6";
                Hero.HeavyAttack = "Whirlwind,1d20";
                Hero.Defend = "Brace,1d12";
                Hero.Heal = "Resolve,1d12";
                Console.WriteLine("You have chosen Warrior has your class. You received a +1 bonus to STR.");
                break;
                case "Paladin":
                Hero.charClass = "Paladin";
                Hero.Con += 1;
                Hero.Attack = "Thrust,3d6";
                Hero.HeavyAttack = "Impale,1d20";
                Hero.Defend = "Shield Wall,1d12";
                Hero.Heal = "Holy Light,1d12";
                Console.WriteLine("You have chosen Paladin as your class. You received a +1 bonus to CON.");
                break;
                case "Mage":
                Hero.charClass = "Mage";
                Hero.Int += 1;
                Hero.Attack = "Magic Missles,3d6";
                Hero.HeavyAttack = "Fireball,1d20";
                Hero.Defend = "Magic Shield,1d12";
                Hero.Heal = "Innervate,1d12";
                Console.WriteLine("You have chosen Mage as your class. You received a +1 bonus to INT.");
                break;
                case "Warlock":
                Hero.charClass = "Warlock";
                Hero.Wis += 1;
                Hero.Attack = "Dark Bolt,3d6";
                Hero.HeavyAttack = "Mind Blast,1d20";
                Hero.Defend = "Demon Shield,1d12";
                Hero.Heal = "Syphon,1d12";
                Console.WriteLine("You have chosen Warlock as your class. You received a +1 bonus to WIS.");
                break;
                default:
                Console.WriteLine("Invalid input.");
                SetClass();
                break;
            }
        }
    }
}